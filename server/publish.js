Meteor.publish('system', function() {
	return IPCDB.find({_id:"system"}, 
  {fields : {
    "pwr_front": 1,
    "pwr_rear": 1,
    "license": 1,
    "time":1,
    "frameid":1,
    "alarm":1,
    "release":1,
    "username":1
  }});
});
