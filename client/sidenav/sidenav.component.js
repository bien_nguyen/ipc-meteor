angular.module('rt600').directive('sideNav', function() {
	return {
		restrict: 'E',
		templateUrl: 'client/sidenav/side-nav.html',
		controllerAs: 'sideNav',
		controller: function ($scope, $reactive) {
			$reactive(this).attach($scope);

			this.cfgToggle = false;
			this.provToggle = false;
			this.mtnToggle = false;
			this.repToggle = false;
			this.usrmngToggle = false;
			this.ipcadmToggle = false;

			this.toggleCfg = () => {
				this.cfgToggle = this.cfgToggle === true ? false: true;
			};
			this.toggleProv = () => {
				this.provToggle = this.provToggle === true ? false: true;
			};
			this.toggleMtn = () => {
				this.mtnToggle = this.mtnToggle === true ? false: true;
			};
			this.toggleRep = () => {
				this.repToggle = this.repToggle === true ? false: true;
			};
			this.toggleUsrMng = () => {
				this.usrmngToggle = this.usrmngToggle === true ? false: true;
			};
			this.toggleIpcAdm = () => {
				this.ipcadmToggle = this.ipcadmToggle === true ? false: true;
			};

			this.cfgArrow = () => {
				if (this.cfgToggle == true) {
					return "hardware:ic_keyboard_arrow_up_24px";
				} else if (this.cfgToggle == false) {
					return "hardware:ic_keyboard_arrow_down_24px";
				}
			};
			this.provArrow = () => {
				if (this.provToggle == true) {
					return "hardware:ic_keyboard_arrow_up_24px";
				} else if (this.provToggle == false) {
					return "hardware:ic_keyboard_arrow_down_24px";
				}
			};
			this.mtnArrow = () => {
				if (this.mtnToggle == true) {
					return "hardware:ic_keyboard_arrow_up_24px";
				} else if (this.mtnToggle == false) {
					return "hardware:ic_keyboard_arrow_down_24px";
				}
			};
			this.repArrow = () => {
				if (this.repToggle == true) {
					return "hardware:ic_keyboard_arrow_up_24px";
				} else if (this.repToggle == false) {
					return "hardware:ic_keyboard_arrow_down_24px";
				}
			};
			this.usrmngArrow = () => {
				if (this.usrmngToggle == true) {
					return "hardware:ic_keyboard_arrow_up_24px";
				} else if (this.usrmngToggle == false) {
					return "hardware:ic_keyboard_arrow_down_24px";
				}
			};
			this.ipcadmArrow = () => {
				if (this.ipcadmToggle == true) {
					return "hardware:ic_keyboard_arrow_up_24px";
				} else if (this.ipcadmToggle == false) {
					return "hardware:ic_keyboard_arrow_down_24px";
				}
			};
		}
	}
});

