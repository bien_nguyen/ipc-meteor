angular.module('rt600').directive('topzone', function() {
	return {
		restrict: 'E',
		templateUrl: 'client/topzone/topzone.html',
		controllerAs: 'topzone',
		controller: function ($scope, $reactive) {
			$reactive(this).attach($scope);

			this.subscribe('system');

			this.helpers({
				system: () => {
					console.log(IPCDB.findOne());
					return IPCDB.findOne();
				}
			});
		}
	}
});
