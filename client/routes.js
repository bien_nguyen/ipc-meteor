angular.module('rt600').config(function ($urlRouterProvider, $stateProvider, $locationProvider) {
	//$locationProvider.html5Mode(true);

	$stateProvider
		.state('warning', {
			url: '/',
			template: '<warning></warning>'
		})
		.state('mainpage', {
			template: '<main-page></main-page>'
		})
		.state('mio1', {
			url: '/mio1',
			template: '<mio1></mio1>'
		})
		.state('ipc', {
			url: '/ipc',
			template: '<ipc></ipc>'
		})
		.state('wire-center', {
			url:'/wc',
			template: '<wire-center></wire-center>'
		});

	$urlRouterProvider.otherwise("/");
});

angular.module('rt600').run(function($rootScope, $state) {
	$rootScope.$watch(
		function () {
			var userId = $rootScope.currentUser;
			return userId;
		},
		function (newValue, oldValue) {
			if (!newValue) {
				$state.go('warning');
			}
			else if ($state.is('warning') && newValue) {
				$state.go('mainpage');
			}
		}
	);
});
