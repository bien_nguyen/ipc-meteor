angular.module('rt600').directive('ipc', function() {
	return {
		restrict: 'E',
		templateUrl: 'client/views/ipc/ipc.html',
		controllerAs: 'ipcctrl',
		controller: function($scope, $reactive, ipcService) {
			$reactive(this).attach($scope);

			this.pwrobj = {
				id: "system"
			};
			if (!this.pwrobj.hasOwnProperty('front')) {
				console.log('front does not exist');
			}

			this.frontsubmit = (stat) => {
				this.pwrobj.side = 'pwr_front';
				this.pwrobj.value = stat;
				ipcService.setPwr(this.pwrobj);
			};

			this.rearsubmit = () => {
				ipcService.setPwr(this.pwrobj);
			};

			this.pwrsubmit = () => {
				ipcService.setPwr(this.pwrobj);
			};

			//var reactivefront = new ReactiveVar('');

			this.helpers({
				pwrfront: () => {
					let id = "system";
					//reactivefront.set(ipcService.getPwrFront(id));
					return (ipcService.getPwrFront(id));
				},
				pwrrear: () => {
					let id = "system";
					let result = ipcService.getPwrRear(id);
					return result;
					//return (IPCDB.findOne({_id:"system"}).pwr_rear);
				}
			});

			console.log(IPCDB.findOne({}));
			console.log("test compile");
		}
	}
});

