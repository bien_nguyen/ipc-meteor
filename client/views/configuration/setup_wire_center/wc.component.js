angular.module('rt600').directive('wireCenter', function() {
	return {
		restrict: 'E',
		templateUrl: 'client/views/configuration/setup_wire_center/wc.html',
		controllerAs: 'wireCenter',
		controller: function ($scope, $reactive) {
			$reactive(this).attach($scope);
		}
	}
});
